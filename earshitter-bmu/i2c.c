/*
 * This file is part of the earshitter project.
 *
 * Copyright (C) 2020 Gordon Quad
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this library.  If not, see <http://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE
#include <libopencm3/cm3/common.h>
#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/usart.h>
#include <libopencm3/stm32/i2c.h>
#include <libopencm3/cm3/nvic.h>
#include <stdio.h>
#include <errno.h>
#include <stddef.h>
#include <sys/types.h>

static ssize_t _iord(void *_cookie, char *_buf, size_t _n);
static ssize_t _iowr(void *_cookie, const char *_buf, size_t _n);

static ssize_t _iord(void *_cookie, char *_buf, size_t _n)
{
    /* dont support reading now */
    (void)_cookie;
    (void)_buf;
    (void)_n;
    return 0;
}

static ssize_t _iowr(void *_cookie, const char *_buf, size_t _n)
{
    uint32_t dev = (uint32_t)_cookie;

    int written = 0;
    while (_n-- > 0) {
        usart_send_blocking(dev, *_buf++);
        written++;
    };
    return written;
}

#define I2C_ADDRESS 0x34

#define INT_PORT GPIOA
#define INT_PIN GPIO7

uint8_t am = 0;

bool reg_read = false;
uint8_t i2c_register;
uint8_t regs[256];
bool key_read = false;

void i2c1_isr(void) {
    static int i=0;

    uint32_t isr = I2C_ISR(I2C1);

    if (isr & I2C_ISR_RXNE) {
        //do this first so we don't lose a char if other events are happening
        volatile uint8_t byte = i2c_get_data(I2C1);
        if (!reg_read) {
            i2c_register = byte;
            reg_read = true;
        }
        else
        {
            if (i2c_register == 0x02)
            {
                regs[i2c_register++] |= ~byte;
                regs[0x02] &= 0x1f;
                gpio_set(GPIOA, GPIO7);
            }
            else
                regs[i2c_register++] = byte;
        }
    }

    if (isr & I2C_ISR_ADDR) {
        am = 1;
        I2C_ICR(I2C1) = I2C_ICR_ADDRCF;
        if (isr & I2C_ISR_DIR_READ) {
            I2C_ISR(I2C1) |= I2C_ISR_TXE;
        }
    }

    if ((isr & (I2C_ISR_TXIS | I2C_ISR_DIR_READ)) == (I2C_ISR_TXIS | I2C_ISR_DIR_READ)) {
        if(!(isr & I2C_ISR_STOPF)) {
            volatile uint8_t byte = regs[i2c_register];
            if (i2c_register == 0x04)
            {
                if (!key_read)
                {
                    key_read = true;
                    i2c_send_data(I2C1, byte);
                    gpio_set(GPIOA, GPIO7);
                }
                else
                {
                    i2c_send_data(I2C1, 0x00);
                }
            }
            else
                i2c_send_data(I2C1, byte);
            ++i2c_register;
        }
    }

    if (isr & I2C_ISR_STOPF) {
        I2C_ICR(I2C1) = I2C_ICR_STOPCF;
        i2c_send_data(I2C1, 0); //hack to avoid another txis interrupt at the end
        reg_read = false;
    }

    if (isr & I2C_ISR_NACKF) {
        I2C_ICR(I2C1) = I2C_ICR_NACKCF;
        reg_read = false;
    }
}

static void clock_setup(void)
{
    rcc_clock_setup_in_hsi_out_48mhz();
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_GPIOB);
    rcc_periph_clock_enable(RCC_USART1);
    rcc_periph_clock_enable(RCC_I2C1);
    rcc_set_i2c_clock_hsi(I2C1);
}

static void i2c_setup(void)
{
    i2c_reset(I2C1);
    i2c_peripheral_disable(I2C1);
    i2c_set_prescaler(I2C1, 1);
    i2c_set_7bit_addr_mode(I2C1);
    i2c_set_own_7bit_slave_address(I2C1, I2C_ADDRESS);
    I2C_OAR1(I2C1) |= I2C_OAR1_OA1EN_ENABLE;
    i2c_set_speed(I2C1, i2c_speed_sm_100k, 8);
    i2c_enable_stretching(I2C1);
    i2c_peripheral_enable(I2C1);
    i2c_enable_interrupt(I2C1, I2C_CR1_ADDRIE | I2C_CR1_RXIE | I2C_CR1_TXIE | I2C_CR1_STOPIE | I2C_CR1_TCIE);
    nvic_enable_irq(NVIC_I2C1_IRQ);
}

static FILE *usart_setup(void)
{
    usart_set_baudrate(USART1, 115200);
    usart_set_databits(USART1, 8);
    usart_set_parity(USART1, USART_PARITY_NONE);
    usart_set_stopbits(USART1, USART_CR2_STOPBITS_1);
    usart_set_mode(USART1, USART_MODE_TX);
    usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);

    usart_enable(USART1);

    cookie_io_functions_t stub = { _iord, _iowr, NULL, NULL };
    FILE *fp = fopencookie((void *)USART1, "rw+", stub);
    /* Do not buffer the serial line */
    setvbuf(fp, NULL, _IONBF, 0);
    return fp;
}

static void gpio_setup(void)
{
    gpio_mode_setup(GPIOA, GPIO_MODE_OUTPUT, GPIO_PUPD_PULLUP, GPIO7);
    gpio_set(GPIOA, GPIO7);

    gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO9);
    gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO10);

    gpio_set_af(GPIOA, GPIO_AF1, GPIO9);
    gpio_set_af(GPIOA, GPIO_AF1, GPIO10);

    gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO7);
    gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO6);

    gpio_set_af(GPIOB, GPIO_AF1, GPIO7);
    gpio_set_af(GPIOB, GPIO_AF1, GPIO6);

    gpio_set_output_options(GPIOB, GPIO_OTYPE_OD, GPIO_OSPEED_100MHZ, GPIO7);
    gpio_set_output_options(GPIOB, GPIO_OTYPE_OD, GPIO_OSPEED_100MHZ, GPIO6);
}

int main(void)
{
    int i = 0, j = 0, c = 0;
    uint8_t pressed = 0x0;

    for (i = 0; i < 256; ++i)
        regs[i] = i+1;

    regs[0x01] = 0x00;
    regs[0x02] = 0x00;
    regs[0x03] = 0x00;
    regs[0x04] = 0x07;

    clock_setup();
    gpio_setup();
    FILE *poo = usart_setup();
    i2c_setup();

    /*for (j = 0; j < 6000; j++)
        for (i = 0; i < 100000; i++) {
            __asm__("NOP");
        };*/

    while (1) {
/*        if (i2c_rx_start != i2c_rx_end)
        {
            uint8_t byte = i2c_rx_buf[i2c_rx_start++];
            uint8_t x = byte % 16;
            if (x < 0xa)
                usart_send_blocking(USART1, '0' + x);
            else
                usart_send_blocking(USART1, 'A' + x - 10);
            x = byte / 0x10;
            if (x < 0xa)
                usart_send_blocking(USART1, '0' + x);
            else
                usart_send_blocking(USART1, 'A' + x - 10);
            usart_send_blocking(USART1, '\r');
            usart_send_blocking(USART1, '\n');
        }*/
        if ((c % 20) == 0)
        {
            pressed ^= 0x80;
            regs[0x04] = j | pressed;
            if (!pressed)
                ++j;
            if (j > 40)
                j = 0;
            regs[0x02] |= 0x1;
            key_read = false;
            gpio_clear(GPIOA, GPIO7);
        }
        fprintf(poo, "poo %d %d \r\n", c, am);
        am = 0;
        ++c;
        for (i = 0; i < 100000; i++) {
            __asm__("NOP");
        }
    }

    return 0;
}
