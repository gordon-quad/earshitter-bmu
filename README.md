Easy "clone and go" repository for a libopencm3 based project.

# Instructions
 1. git clone --recurse-submodules https://gitlab.com/gordon-quad/earshitter-bmu.git earshitter-bmu
 2. cd earshitter-bmu
 3. make -C libopencm3 # (Only needed once)
 4. make -C earshitter-bmu

If you have an older git, or got ahead of yourself and skipped the ```--recurse-submodules```
you can fix things by running ```git submodule update --init``` (This is only needed once)
